FROM node:8-alpine
WORKDIR /usr/src/app
COPY . .
EXPOSE 80
CMD [ "node", "app.js" ]
